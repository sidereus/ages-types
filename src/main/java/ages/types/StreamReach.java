
package ages.types;

/**
 *
 * @author Nathan Lighthart
 */
public class StreamReach {
    public int ID;
    public double length;
    public double slope;
    public double rough;
    public double width;
    public double deepsink;

    public StreamReach() {
        ID = -1;
        length = 1;
        slope = 1;
        rough = 1;
        width = 1;
        deepsink = 0;
    }
}
