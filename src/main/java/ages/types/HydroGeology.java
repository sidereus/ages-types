
package ages.types;

/**
 *
 * @author Nathan Lighthart
 */
public class HydroGeology {
    public int GID;
    public double RG1_max;
    public double RG2_max;
    public double RG1_k;
    public double RG2_k;
    public double RG1_active;
    public double Kf_geo;
}
