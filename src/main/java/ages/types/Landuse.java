
package ages.types;

/**
 *
 * @author Nathan Lighthart
 */
public class Landuse {
    public int LID;
    public double albedo;
    public double[] RSC0;
    public double[] LAI;
    public double[] effHeight;
    public double rootDepth;
    public double sealedGrade;
    public double cFactor;
}
