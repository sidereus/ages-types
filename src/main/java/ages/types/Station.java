
package ages.types;

/**
 *
 * @author Nathan Lighthart
 */
public class Station {
    private final double x;
    private final double y;
    private final double elevation;

    public Station(double x, double y, double elevation) {
        this.x = x;
        this.y = y;
        this.elevation = elevation;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getElevation() {
        return elevation;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Station other = (Station) obj;
        return x == other.x && y == other.y && elevation == other.elevation;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (int) (Double.doubleToLongBits(x) ^ (Double.doubleToLongBits(x) >>> 32));
        hash = 37 * hash + (int) (Double.doubleToLongBits(y) ^ (Double.doubleToLongBits(y) >>> 32));
        hash = 37 * hash + (int) (Double.doubleToLongBits(elevation) ^ (Double.doubleToLongBits(elevation) >>> 32));
        return hash;
    }
}
